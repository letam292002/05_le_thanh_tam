@extends('admin.master')
@section('title')
<div class="page-title-icon">
    <i class="pe-7s-car icon-gradient bg-mean-fruit"></i>
</div>
<div>Quản Lý Sản Phẩm
    <div class="page-title-subheading">
        Thêm Mới Sản Phẩm và Quản Lý Các Loại Sản Phẩm
        <button class="btn btn-warning" id="nutNew">Nút gọi hàm cho vui</button>
    </div>
</div>
@endsection
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input type="text" name="" id="idCanXoa" class="form-control" hidden>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
          <button id="taoChinhLaThangXoa" type="button" class="btn btn-danger" data-dismiss="modal">Xóa Sản Phẩm</button>
        </div>
      </div>
    </div>
</div>
@section('content')
<div class="row">
    <div class="col-md-5">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Thêm Mới Sản Phẩm</h5>
                {{-- @if($errors->any())
                    @foreach ($errors->all() as $key => $value)
                    <div class="alert alert-danger" role="alert">
                        {{ $value }}
                    </div>
                    @endforeach
                @endif --}}
                <form autocomplete="off" id="createDanhMuc">
                    <div class="position-relative form-group">
                        <label>Tên Sản Phẩm</label>
                        <input id="ten_san_pham" placeholder="Nhập vào tên sản phẩm" type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label>Slug Sản Phẩm</label>
                        <input id="slug_san_pham" placeholder="Nhập vào slug sản phẩm" type="text" class="form-control">
                    </div>
                    {{-- <div class="position-relative form-group">
                        <label>Số Lượng</label>
                        <input id="so_luong" placeholder="Nhập vào số lượng" type="number" class="form-control">
                    </div> --}}
                    <div class="position-relative form-group">
                        <label>Giá Bán</label>
                        <input id="gia_ban" placeholder="Nhập vào giá bán" type="number" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label>Giá Khuyến Mãi</label>
                        <input id="gia_khuyen_mai" placeholder="Nhập vào giá khuyến mãi" type="number" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label>Ảnh Đại Diện</label>
                        <div class="input-group">
                            <input id="anh_dai_dien" name="anh_dai_dien" class="form-control" type="text">
                            <input type="button" class="btn-info lfm" data-input="anh_dai_dien" data-preview="holder" value="Upload">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;">
                    </div>
                    <div class="position-relative form-group">
                        <label>Mô Tả Ngắn</label>
                        <input id="mo_ta_ngan" placeholder="Nhập vào mô tả ngắn" type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label>Mô Tả Chi Tiết</label>
                        <input id="mo_ta_chi_tiet" placeholder="Nhập vào mô tả chi tiết" type="text" class="form-control">
                    </div>
                    <div class="position-relative form-group">
                        <label>Danh Mục</label>
                        <select id="id_danh_muc"class="form-control">
                            @foreach ($list_danh_muc as $value)
                                <option value={{ $value->id }}>{{ $value->ten_danh_muc }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="position-relative form-group">
                        <label>Tình Trạng</label>
                        <select id="is_open"class="form-control">
                            <option value=1>Hiển Thị</option>
                            <option value=0>Tạm Tắt</option>
                        </select>
                    </div>
                    <button type="button" class="mt-1 btn btn-primary" id="createSanPham">Tạo Mới Sản Phẩm</button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="main-card mb-3 card">
            <div class="card-body"><h5 class="card-title">Table bordered</h5>
                <table class="mb-0 table table-bordered" id="tableSanPham">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Tên Sản Phẩm</th>
                            <th class="text-center">Slug Sản Phẩm</th>
                            {{-- <th class="text-center">Số Lượng</th> --}}
                            <th class="text-center">Giá Bán</th>
                            <th class="text-center">Giá Khuyến Mãi</th>
                            <th class="text-center">Danh Mục</th>
                            <th class="text-center">Tình Trạng</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
<script>
    $('.lfm').filemanager('image');
</script>
<script>
    $(document).click(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $("#createSanPham").click(function(e){
            e.preventDefault();
            var ten_san_pham            = $("#ten_san_pham").val();
            var slug_san_pham           = $("#slug_san_pham").val();
            // var so_luong                = $("#so_luong").val();
            var gia_ban                 = $("#gia_ban").val();
            var gia_khuyen_mai          = $("#gia_khuyen_mai").val();
            var anh_dai_dien            = $("#anh_dai_dien").val();
            var mo_ta_ngan              = $("#mo_ta_ngan").val();
            var mo_ta_chi_tiet          = $("#mo_ta_chi_tiet").val();
            var id_danh_muc             = $("#id_danh_muc").val();
            var is_open                 = $("#is_open").val();

            var thongTinSanPhamCanTao = {
                'ten_san_pham'            :   ten_san_pham,
                'slug_san_pham'           :   slug_san_pham,
                // 'so_luong'                :   so_luong,
                'gia_ban'                 :   gia_ban,
                'gia_khuyen_mai'          :   gia_khuyen_mai,
                'anh_dai_dien'            :   anh_dai_dien,
                'mo_ta_ngan'              :   mo_ta_ngan,
                'mo_ta_chi_tiet'          :   mo_ta_chi_tiet,
                'id_danh_muc'             :   id_danh_muc,
                'is_open'                 :   is_open,
            };

            console.log(thongTinSanPhamCanTao);

            $.ajax({
                url     :   '/admin/san-pham/index',
                type    :   'post',
                data    :   thongTinSanPhamCanTao,
                success :   function(res){
                    if(res.thongBao == true){
                        loadTable();
                    }
                },
                error   :    function(res) {
                    var danh_sach_loi = res.responseJSON.errors;
                    $.each(danh_sach_loi, function(key, value){
                        toastr.error(value[0]);
                    });
                },
            });
        });
        function loadTable(){
            $.ajax({
                url     :   '/admin/san-pham/data',
                type    :   'get',
                success :   function(res) {
                    var html ='';
                    $.each(res.dulieuneban, function(key, value){
                        if(value.is_open == true) {
                            var doan_muon_hien_thi = '<button class="btn btn-primary doiTrangThai" data-id="'+ value.id +'">Hiện thị</button>';
                        } else {
                            var doan_muon_hien_thi  = '<button class="btn btn-danger doiTrangThai" data-id="'+ value.id +'">Tạm tắt</button>';
                        }
                        html += '<tr>';
                        html += '<td scope="row">'+ (key + 1) +'</th>';
                        html += '<td>'+ value.ten_san_pham +'</td>';
                        html += '<td>'+ value.slug_san_pham +'</td>';
                        // html += '<td>'+ value.so_luong +'</td>';
                        html += '<td>'+ value.gia_ban +'</td>';
                        html += '<td>'+ value.gia_khuyen_mai +'</td>';
                        html += '<td>'+ value.ten_danh_muc +'</td>';
                        html += '<th>'+ doan_muon_hien_thi +'</th>';
                        html += '<td>';
                        html += '<button class="btn btn-danger nutDelete" data-tamid="'+ value.id +'" data-toggle="modal" data-target="#exampleModal">Xóa</button>';
                        html += '</td>';
                        html += '</tr>';
                    });

                    $("#tableSanPham tbody").html(html);
                },
            });
        }

        loadTable();



        $('#nutNew').click(function(e){
            loadTable();
        });

        $('body').on('click', '.doiTrangThai', function(){
            var id_cua_em = $(this).data('id');
            $.ajax({
                url     :   '/admin/san-pham/doi-trang-thai/' + id_cua_em,
                type    :   'get',
                success :   function(res) {
                    if(res.status){
                        loadTable();
                    }
                },
            });
        });

        $('body').on('click', '.nutDelete', function(){
            var id_cua_em = $(this).data('tamid');
            $("#idCanXoa").val(id_cua_em);
        });

        $("#taoChinhLaThangXoa").click(function(){
            var id_can_xoa = $("#idCanXoa").val();
            $.ajax({
                url     :   '/admin/san-pham/xoa-san-pham/' + id_can_xoa,
                type    :   'get',
                success :   function(res) {
                    if(res.status){
                        loadTable();
                    }
                },
            });
        });
    });
</script>
@endsection
