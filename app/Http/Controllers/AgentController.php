<?php

namespace App\Http\Controllers;

use App\Http\Requests\AgentRegisterRequest;
use App\Mail\MailKichHoatDaiLy;
use App\Models\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AgentController extends Controller
{
    public function register()
    {
        return view('agent.register');
    }

    public function registerAction(AgentRegisterRequest $request)
    {
        $parts = explode(" ", $request->ho_va_ten);
        if(count($parts) > 1) {
                $lastname = array_pop($parts);
                $firstname = implode(" ", $parts);
            }
            else
            {
                $firstname = $request->ho_va_ten;
                $lastname = " ";
            }
            $data = $request->all();
            $data['hash']       = Str::uuid();
            $data['ho_lot']     = $firstname;
            $data['ten']        = $lastname;
            $data['thanh_pho']  = "Đà Nẵng";
            $data['password']   = bcrypt($request->password);
            Agent::create($data);

            Mail::to($request->email)->send(new MailKichHoatDaiLy(
                $request->ho_va_ten,
                $data['hash'],
                'Kích hoạt tài khoản Đăng Kí',
            ));

        return response()->json(['thongbao'=>true]);
    }
}
